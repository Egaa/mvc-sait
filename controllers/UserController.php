<?php

include_once ROOT. '/models/User.php';

class UserController
{
    public function actionLogin()
    {
        User::checkLoggedForLogin();

        //login button in /views/login/...
        if(isset($_POST["login"]))
        {
            if(!empty($_POST['username']) && !empty($_POST['password'])) 
            {
                $username = $_POST['username'];
                $password = $_POST['password'];

                //Проверяем пользователя на существование
                $username = User::checkUserData($username, $password);
                if($username == false) 
                {
                    $message = "Неверный логин или пароль!";
                }
                else
                {
                    User::auth($username);
                    header("Location: page");
                }
            } 
            else 
            {
                $message = "Все поля обязательны для заполнения!";
            }
        }

        require_once('/views/login/index.php');



        return true;
    }

    public function actionRegister()
    {
        //register button in /views/register/...
        if(isset($_POST["register"]))
        {
            if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password'])) 
            {
                $name=$_POST['name'];
                $email=$_POST['email'];
                $username=$_POST['username'];
                $password=$_POST['password'];
            
                $numrows = User::selAllUsers($username);
                $result = User::createUser($name, $email, $username, $password);

                if($numrows==0)
                {
                    $sql="INSERT INTO users (name, email, username,password) VALUES('$name','$email', '$username', '$password')";
                    $result=mysql_query($sql);

                    if($result)
                    {
                        $msg = "Аккаунт успешно зарегистрирован!";
                    } else 
                    {
                        $err = "Ошибка при регистрации!";
                    }

                } else 
                {
                    $err = "Этот логин уже использован другим! Пожалуйста используйте другой!";
                }

            } else 
            {
                $err = "Все поля обязательны для заполнения!";
            }
        }
        
        require_once('/views/register/index.php');

        return true;
    }
}