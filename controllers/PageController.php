<?php

include_once ROOT. '/models/Page.php';
include_once ROOT. '/models/User.php';

class PageController
{
    /**
     * Вывод дерева
     * @param Integer $parent_id - id-родителя
     * @param Integer $level - уровень вложености
     */
    function outTree($parent_id, $level) {
        global $category_arr; //Делаем переменную $category_arr видимой в функции
        $category_arr = Page::getCategory(); //В переменную $category_arr записываем все категории

        if (isset($category_arr[$parent_id])) { //Если категория с таким parent_id существует
            foreach ($category_arr[$parent_id] as $value) { //Обходим
                /**
                 * Выводим категорию 
                 *  $level * 25 - отступ, $level - хранит текущий уровень вложености (0,1,2..)
                 */
                echo "<div style='margin-left:" . ($level * 25) . "px;'><a href='/page/" . $value['id'] . "'>" . $value["title"] . "</div></a>";

                $level = $level + 1; //Увеличиваем уровень вложености
                //Рекурсивно вызываем эту же функцию, но с новым $parent_id и $level
                $this->outTree($value["id"], $level);
                $level = $level - 1; //Уменьшаем уровень вложености
            }
        }
    }

    public function actionIndex()
    {
        User::checkLogged();

        //logout button in /views/page/...
        if(isset($_POST["logout"]))
        {
            //session_start();
            unset($_SESSION['session_username']);
            session_destroy();
            header("Location: /");
            return;
        }
            
        $categoryList = array();
        $categoryList = Page::getCategoryList();

        require_once('/views/page/index.php');

        return true;
    }

    public function actionView($id)
    {
        User::checkLogged();
        
        if ($id) 
        {
            $categoryItem = Page::getCategorysItemById($id);

            if(isset($_POST["submit"]))
            {
                if(!empty($_POST['title'])) 
                {
                    $title = $_POST['title'];
    
                    $row = Page::updateTitle($id, $title);
                    if($row)
                    {
                        $msg = "Заголовок обновлен!";
                    }
                    else 
                    {
                        $err = "Ошибка выполнения!";
                    }
                } 
                else
                {
                    $err = "Заполните все поля!";
                }
                if(!empty($_POST['content'])) 
                {
                    $content = $_POST['content'];
    
                    $row = Page::updateContent($id, $content);
                    if($row)
                    {
                        $msg = "Текст обновлен!";
                    }
                    else 
                    {
                        $err = "Ошибка выполнения!";
                    }
                }
                else
                {
                    $err = "Заполните все поля!"; 
                }
            }

            require_once('/views/view/index.php');
        }

        return true;
    }
}