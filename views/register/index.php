<?php include("includes/header.php"); ?>

<main class="container mregister">
	<div id="login">
		<h1>Регистрация</h1>
		<form name="registerform" id="registerform" action="" method="post">
			<p><label for="user_login">Имя<br />
				<input type="text" name="name" id="name" class="input" size="32" value=""  />
			</label></p>
			
			<p><label for="user_pass">Email<br />
				<input type="email" name="email" id="email" class="input" value="" size="32" />
			</label></p>

			<p><label for="user_pass">Логин<br />
				<input type="text" name="username" id="username" class="input" value="" size="20" />
			</label></p>
			
			<p><label for="user_pass">Пароль<br />
				<input type="password" name="password" id="password" class="input" value="" size="32" />
			</label></p>

			<p class="submit">
				<input type="submit" name="register" id="register" class="button" value="Регистрация" />
			</p>

			<p class="regtext">Уже зарегистрирован? <a href="/" >Авторизоваться</a>!</p>
		</form>
	</div>
</main>

<?php if (!empty($err)) {echo "<p class=\"error\">" . "Ошибка: ". $err . "</p>";} ?>
<?php if (!empty($msg)) {echo "<p class=\"msg\">" . "Поздравляем! ". $msg . "</p>";} ?>

<?php include("includes/footer.php"); ?>