<?php include("includes/header.php"); ?>

<main class="container mlogin">
    <div id="login">
        <h1>Авторизация</h1>
        <form name="loginform" id="loginform" action="" method="POST">
            <p>
                <label for="user_login">Логин<br />
                    <input type="text" name="username" id="username" class="input" value="" size="20" />
                </label>
            </p>
            <p>
                <label for="user_pass">Пароль<br />
                    <input type="password" name="password" id="password" class="input" value="" size="20" />
                </label>
            </p>
                <p class="submit">
                <input type="submit" name="login" class="button" value="Войти" /> 
            </p>
            <p class="regtext">Нету аккаунта? <a href="/register" >Зарегистрироваться</a>!</p>
        </form>
    </div>
</main>

<?php if (!empty($message)) {echo "<p class=\"error\">" . "Ошибка: ". $message . "</p>";} ?>

<?php include("includes/footer.php"); ?>