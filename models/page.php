<?php

class Page
{
    /**
     * returns single news item with specified id
     * @param ineger $id
     */
    public static function getCategorysItemById($id)
    {
        $id = intval($id);

        if ($id) {

            $db = Db::getConnection();
            $query = mysql_query('SELECT * FROM news WHERE id=' . $id);

            $categoryItem = mysql_fetch_assoc($query);

            return $categoryItem;
        }
    }

    /**
     * returns an array of news items
     */
    public static function getCategoryList()
    {
        $categoryList = array();

        $db = Db::getConnection();
        $query = mysql_query('SELECT id, parent_id, title FROM news ');

        $numrows = mysql_num_rows($query);

        $i = 0;
        if($numrows != 0)
        {
            while($row = mysql_fetch_assoc($query))
            {
                $categoryList[$i]['id'] = $row['id'];
                $categoryList[$i]['parent_id'] = $row['parent_id'];
                $categoryList[$i]['title'] = $row['title'];
                $i++;
            }
        }

         return $categoryList;
	}
	
	public static function getCategory() 
	{ 
        $db = Db::getConnection();

		$query = mysql_query("SELECT * FROM `news`");

		$result = array();
		
		while ($row = mysql_fetch_array($query)) 
		{
            $result[$row["parent_id"]][] = $row;
        }
        return $result;
	}

	public static function updateTitle($id, $title)
    {
		$id = intval($id);

        $db = Db::getConnection();

		$query=mysql_query("UPDATE news SET title='".$title."' WHERE id='".$id."'");

		return true;
	}
	
	public static function updateContent($id, $content)
    {
		$id = intval($id);

        $db = Db::getConnection();

		$query=mysql_query("UPDATE news SET content='".$content."' WHERE id='".$id."'");

		return true;
    }
}
?>