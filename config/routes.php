<?php
return array(
    // '' - введённый адрес => '' - Name в actionName в /controllers/ 

    //'news/([a-z]+)/([0-9]+)' => 'news/view/$1/$2',
    'login' => 'user/login', // actionLogin in UserController
    'register' => 'user/register', // actionRegister in UserController
    'page/([0-9]+)' => 'page/view/$1/', //actionView in PageConroller
    'page' => 'page/index', // actionIndex in PageController
);
?>