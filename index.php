<?php 

    // FRONT CONTROLLER
    session_start();

    // 1. Общие настройки
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //2. Подключение файлов сис-мы
    define('ROOT', dirname(__FILE__));
    require_once(ROOT.'/components/Router.php');

    //3. Установка соединения с БД
    require_once(ROOT. '/components/Db.php');

    //4. Вызов Router
    $router = new Router();
    $router->run();