<?php
/**
 * Класс Router
 * Компонент для работы с маршрутами
 */
class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Returns request string
     */
    private function getURI()
    {
        // Получить строку запроса
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        //Получить строку запроса
        $uri = $this->getURI();
        if($uri == "")
            $uri = 'login';
        //Проверить наличие такого запроса в router.php
        foreach ($this->routes as $uriPattern => $path) {
            
            //Сравниваем $uriPattern и $uri и если есть совпадение, продолжаем
            if (preg_match("~$uriPattern~", $uri)) {
                
                //Получаем внутрений путь из внешнего согласно правилу
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                
                //определить  контроллер, action, параметры
                $segments = explode('/', $internalRoute);
                
                $controllerName = array_shift($segments).'Controller';
                // ucfirst делаем первую букву заглавную
                $controllerName = ucfirst($controllerName);
                
                $actionName = 'action'.ucfirst(array_shift($segments));

                $parameters = $segments;

                //Подключить файл класса-котролера
                $controllerFile = ROOT . '/controllers/' .
                    $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                // Создать объект, вызвать метод (т.е action)
                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null) {
                    break;
                }
            }
        }
    }
}
?>